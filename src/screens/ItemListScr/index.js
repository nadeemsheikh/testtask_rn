import React, { Component } from 'react';
import {
    Image,
    StyleSheet,
    View,
    FlatList,
    TouchableOpacity,
    Text
} from 'react-native';
//Header Component
import Header from '../../components/Header'
import DataList from './../../utils/test_data.json'
import {
    AdMobBanner
} from 'react-native-admob'

export default class ProductDetailsScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeIndex: 0,
            listData: DataList,
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Header
                    hearderText={'Home'}
                />
                <View style={styles.listViewCont}>
                    <FlatList data={this.state.listData}
                        keyExtractor={item => item.id}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        extraData={this.state}
                        contentContainerStyle={styles.flatListStyle}
                        ref={ref => {
                            this.flatListRef = ref;
                        }}
                        renderItem={({ item, index }) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate('DetailScreen', {
                                            data: item
                                        })
                                    }}
                                    style={styles.listItem} >
                                    {item.type === 'IMAGE' ?
                                        <Image source={{ uri: item.image }}
                                            style={styles.listImg}
                                            resizeMode={'cover'} />
                                        :
                                        <Text style={styles.listText}>{item.text}</Text>
                                    }
                                </TouchableOpacity>
                            )
                        }}
                    />
                </View>
                <View style={{position:'absolute',bottom:0}}>
                <AdMobBanner
                    adSize="fullBanner"
                    adUnitID="ca-app-pub-3940256099942544/6300978111"
                    testDevices={[AdMobBanner.simulatorId]}
                    onAdFailedToLoad={error => console.error(error)}
                />
                </View>
            </View >
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    flatListStyle: {
        width: '100%',
        paddingBottom: 220
    },
    listViewCont: {
        backgroundColor: '#FAFAFA',
        width: '100%',
    },
    listItem: {
        width: '90%',
        alignSelf: 'center',
        borderRadius: 20,
        backgroundColor: 'white',
        marginTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 12,
    },
    listText: {
        width: '100%',
        height: 200,
        textAlign: 'center',
        padding: 25,
        fontSize: 16
    },
    listImg: {
        width: '100%',
        height: 200,
        borderRadius: 20
    }
})