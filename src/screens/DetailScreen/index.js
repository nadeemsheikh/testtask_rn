import React, { Component } from 'react';
import {
    Image,
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
//Header Component
import Header from '../../components/Header'
import Icons from '../../assets/icons'
export default class ScannerScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            detailData: ''
        }
    }
    componentDidMount() {
        if (this.props.route.params) {
            this.setState({
                detailData: this.props.route.params.data
            })
        }
    }

    render() {
        const { loading } = this.state
        return (
            <View style={styles.container}>
                <Header
                    hearderText={'Detail'}
                    leftIcon={Icons.backArrow}
                    onLeftAction={() => this.props.navigation.goBack()}
                />
                    <Text style={styles.listText}>{this.state.detailData.text}</Text>
                    <Image source={{ uri: this.state.detailData.image }}
                        style={styles.listImg}
                        resizeMode={'cover'} />
                
                {loading &&
                    <View style={[StyleSheet.absoluteFill, { alignItems: 'center', justifyContent: 'center' }]}>
                        <ActivityIndicator animating={loading} size={"large"} />
                    </View>
                }
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
       
    },
    listText: {
        width: '100%',
        padding: 25,
        fontSize: 16
    },
    listImg: {
        width: '90%',
        height: 200,
        marginTop:25,
        marginTop:0,
        alignSelf:'center'
    }
})